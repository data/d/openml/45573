# OpenML dataset: HotpotQA_distractor

https://www.openml.org/d/45573

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

HotpotQA is a new dataset with 113k Wikipedia-based question-answer pairs with four key features: (1) the questions require finding and reasoning over multiple supporting documents to answer; (2) the questions are diverse and not constrained to any pre-existing knowledge bases or knowledge schemas; (3) we provide sentence-level supporting facts required for reasoning, allowingQA systems to reason with strong supervision and explain the predictions; (4) we offer a new type of factoid comparison questions to test QA systems' ability to extract relevant facts and perform necessary comparison. The dataset is taken from https://huggingface.co/datasets/hotpot_qa and this upload is the 'distractor' subset.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/45573) of an [OpenML dataset](https://www.openml.org/d/45573). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/45573/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/45573/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/45573/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

